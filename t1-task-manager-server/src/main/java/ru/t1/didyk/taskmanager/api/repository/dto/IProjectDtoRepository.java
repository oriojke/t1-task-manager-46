package ru.t1.didyk.taskmanager.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.model.ProjectDTO;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {

    @NotNull
    ProjectDTO create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    ProjectDTO create(@NotNull String userId, @NotNull String name);

}
