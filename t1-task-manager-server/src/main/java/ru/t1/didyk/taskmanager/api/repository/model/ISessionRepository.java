package ru.t1.didyk.taskmanager.api.repository.model;

import ru.t1.didyk.taskmanager.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
