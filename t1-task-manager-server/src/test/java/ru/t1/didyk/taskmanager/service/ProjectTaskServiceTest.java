package ru.t1.didyk.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.marker.UnitCategory;
import ru.t1.didyk.taskmanager.service.dto.ProjectDtoService;
import ru.t1.didyk.taskmanager.service.dto.TaskDtoService;

import static ru.t1.didyk.taskmanager.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final ProjectDtoService projectService = new ProjectDtoService(connectionService);

    @NotNull
    private final TaskDtoService taskService = new TaskDtoService(connectionService);

    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @Before
    public void before() {
        taskService.add(USER_TASK);
        taskService.add(ADMIN_TASK);
        projectService.add(USER_PROJECT);
        projectService.add(ADMIN_PROJECT);
    }

    @After
    public void after() {
        taskService.clear(USER1.getId());
        taskService.clear(USER2.getId());
        projectService.clear(USER1.getId());
        projectService.clear(USER2.getId());
    }

    @Test
    public void bindTaskToProjectTest() {
        projectTaskService.bindTaskToProject(USER2.getId(), ADMIN_PROJECT.getId(), ADMIN_TASK.getId());
        Assert.assertEquals(ADMIN_PROJECT.getId(), ADMIN_TASK.getProjectId());
    }

    @Test
    public void removeProjectByIdTest() {
        projectTaskService.removeProjectById(USER1.getId(), USER_PROJECT.getId());
        Assert.assertTrue(projectService.findOneById(USER1.getId(), USER_PROJECT.getId()) == null);
    }

    @Test
    public void unbindTaskFromProjectTest() {
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER_PROJECT.getId(), USER_TASK.getId());
        Assert.assertNotEquals(USER_PROJECT.getId(), USER_TASK.getProjectId());
    }

}
