package ru.t1.didyk.taskmanager.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class ProjectChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private Status status;

    public ProjectChangeStatusByIdRequest(@Nullable String token) {
        super(token);
    }
}
