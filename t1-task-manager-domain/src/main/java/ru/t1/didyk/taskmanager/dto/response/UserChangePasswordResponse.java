package ru.t1.didyk.taskmanager.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public class UserChangePasswordResponse extends AbstractUserResponse {
    public UserChangePasswordResponse(@Nullable UserDTO user) {
        super(user);
    }
}
